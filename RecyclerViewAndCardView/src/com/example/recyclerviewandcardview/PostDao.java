package com.example.recyclerviewandcardview;

import java.util.ArrayList;
import java.util.List;

public class PostDao {

    private final static int[] imgResIds = {
        R.drawable.img_1,
        R.drawable.img_2,
        R.drawable.img_3,
        R.drawable.img_4,
        R.drawable.img_5,
        R.drawable.img_6,
        R.drawable.img_7,
        R.drawable.img_8
    };

    private final static int[] cardBackgroundColors = {
        0xffe51c23,
        0xfff50057,
        0xffab47bc,
        0xff03a9f4,
        0xff009688,
        0xff259b24,
        0xffff9800,
        0xffff5722
    };

    public static List<Post> getPostList() {
        List<Post> postList = new ArrayList<Post>();
        for (int n = 0; n < imgResIds.length; n++) {
            Post post = new Post();
            post.setTitle("This is a title");
            post.setSummary("Welcome to Android Design, your place for learning how to design exceptional Android apps. ");
            post.setImgResId(imgResIds[n]);
            post.setBackgroundColor(cardBackgroundColors[n]);
            postList.add(post);
        }
        return postList;
    }

}
